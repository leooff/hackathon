package br.com.stefanini.controller;

import java.util.ArrayList;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.stefanini.client.RestClient;
import br.com.stefanini.client.model.AlteraLampadaRequest;
import br.com.stefanini.client.model.ConsultarStatusResponse;
import br.com.stefanini.client.model.IndicadoresSensor;

@Controller
public class ApplicationController {

	String[] v = new String[] {"on","off"};
	
	@Autowired
	private RestClient restClient;
	
	@GetMapping("/")
	public String index(Model model) {
		/*ConsultarStatusResponse status = restClient.consultarStatusGeral();
		model.addAttribute("temperatura",status.getDhtTemp());
		model.addAttribute("umidade",status.getDhtUmid());
		model.addAttribute("luminosidade",status.getLdrLum());
		model.addAttribute("l1",status.isLampada1() ? "on" : "off");
		model.addAttribute("l2",status.isLampada2() ? "on" : "off");
		model.addAttribute("l3",status.isLampada3() ? "on" : "off");
		model.addAttribute("l4",status.isLampada4() ? "on" : "off");
		model.addAttribute("l5",status.isLampada5() ? "on" : "off");
		model.addAttribute("l6",status.isLampada6() ? "on" : "off");
		model.addAttribute("l7",status.isLampada7() ? "on" : "off");
		model.addAttribute("l8",status.isLampada8() ? "on" : "off");	*/
		
		model.addAttribute("temperatura","80");
		model.addAttribute("umidade","100");
		model.addAttribute("luminosidade","500");
		model.addAttribute("l1",v[new Random().nextInt(2)]);
		model.addAttribute("l2",v[new Random().nextInt(2)]);
		model.addAttribute("l3",v[new Random().nextInt(2)]);
		model.addAttribute("l4",v[new Random().nextInt(2)]);
		model.addAttribute("l5",v[new Random().nextInt(2)]);
		model.addAttribute("l6",v[new Random().nextInt(2)]);
		model.addAttribute("l7",v[new Random().nextInt(2)]);
		model.addAttribute("l8",v[new Random().nextInt(2)]);	
		
		return "index"; 
	}
	
	@GetMapping("/atualizar")
	public String atualizarSensores(Model model) {
		/*ConsultarStatusResponse status = restClient.consultarStatusGeral();
		model.addAttribute("temperatura",status.getDhtTemp());
		model.addAttribute("umidade",status.getDhtUmid());
		model.addAttribute("luminosidade",status.getLdrLum());
		model.addAttribute("l1",status.isLampada1() ? "on" : "off");
		model.addAttribute("l2",status.isLampada2() ? "on" : "off");
		model.addAttribute("l3",status.isLampada3() ? "on" : "off");
		model.addAttribute("l4",status.isLampada4() ? "on" : "off");
		model.addAttribute("l5",status.isLampada5() ? "on" : "off");
		model.addAttribute("l6",status.isLampada6() ? "on" : "off");
		model.addAttribute("l7",status.isLampada7() ? "on" : "off");
		model.addAttribute("l8",status.isLampada8() ? "on" : "off");	*/
		
		model.addAttribute("temperatura","80");
		model.addAttribute("umidade","100");
		model.addAttribute("luminosidade","500");
		model.addAttribute("l1",v[new Random().nextInt(2)]);
		model.addAttribute("l2",v[new Random().nextInt(2)]);
		model.addAttribute("l3",v[new Random().nextInt(2)]);
		model.addAttribute("l4",v[new Random().nextInt(2)]);
		model.addAttribute("l5",v[new Random().nextInt(2)]);
		model.addAttribute("l6",v[new Random().nextInt(2)]);
		model.addAttribute("l7",v[new Random().nextInt(2)]);
		model.addAttribute("l8",v[new Random().nextInt(2)]);	

		return "index :: sensores";
	}
	
	
	
	@PostMapping("/acenderLampada1")
	public @ResponseBody String ascenderLampada() {
		//AlteraLampadaRequest l = new AlteraLampadaRequest();
		//l.setIdLampada(1);
		//restClient.alterarStatusLampada(l);
		
		return null;
	}
	
	@PostMapping("/PesquisarHistorico")
	public ModelAndView historico(@RequestParam String sensor,@RequestParam String dataInicio,@RequestParam String dataFim) {
		
		ArrayList<IndicadoresSensor> indicadores = new ArrayList<>();
		IndicadoresSensor indicador = new IndicadoresSensor();
		indicador.setComponente("Umidade");
		indicador.setData_hora("12/05/18");
		indicador.setStatus("50%");
		indicadores.add(indicador);
		
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("indicadores", indicadores);
		
	  return mav;
	}

	
}
