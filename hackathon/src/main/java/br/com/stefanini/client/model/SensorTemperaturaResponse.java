package br.com.stefanini.client.model;

public class SensorTemperaturaResponse {

	private Integer temperatura;

	public Integer getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}

	@Override
	public String toString() {
		return "SensorTemperaturaResponse [temperatura=" + temperatura + "]";
	}	
}
