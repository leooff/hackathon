package br.com.stefanini.client.model;

public class AlteraLampadaRequest {

	private int idLampada;

	public int getIdLampada() {
		return idLampada;
	}

	public void setIdLampada(int idLampada) {
		this.idLampada = idLampada;
	}	
}
