package br.com.stefanini.client.model;

public class AlteraLampadaResponse {

	private String estadoLampada;

	public String getEstadoLampada() {
		return estadoLampada;
	}

	public void setEstadoLampada(String estadoLampada) {
		this.estadoLampada = estadoLampada;
	}

	@Override
	public String toString() {
		return "AlteraLampadaResponse [estadoLampada=" + estadoLampada + "]";
	}

}
