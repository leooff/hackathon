package br.com.stefanini.client.model;

import java.util.List;

public class ConsultaIndicadoresResponse {

	private String sensor;

	private String dataInicio;

	private String dataFim;

	private List<IndicadoresSensor> indicadores;	

	public String getSensor() {
		return sensor;
	}

	public void setSensor(String sensor) {
		this.sensor = sensor;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public List<IndicadoresSensor> getIndicadores() {
		return indicadores;
	}

	public void setIndicadores(List<IndicadoresSensor> indicadores) {
		this.indicadores = indicadores;
	}

	@Override
	public String toString() {
		return "ConsultaIndicadoresResponse [sensor=" + sensor + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim
				+ ", indicadores=" + indicadores + "]";
	}
}
