package br.com.stefanini.client.model;

public class SensorTemperaturaLm35Response {

	private Double temperatura;

	public Double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}

	@Override
	public String toString() {
		return "SensorTemperaturaLm35Response [temperatura=" + temperatura + "]";
	}	
}
