package br.com.stefanini.client.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class IndicadoresSensor {

	private Integer id;

	private String componente;
	
	private String data_hora;
	
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getComponente() {
		return componente;
	}

	public void setComponente(String componente) {
		this.componente = componente;
	}

	public String getData_hora() {
		return data_hora;
	}

	public void setData_hora(String data_hora) {
		this.data_hora = data_hora;
	}
	
	public void setData_hora(Date data_hora) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		this.data_hora = sdf.format(data_hora);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "IndicadoresSensor [id=" + id + ", componente=" + componente + ", data_hora=" + data_hora + ", status="
				+ status + "]";
	}
	
}
