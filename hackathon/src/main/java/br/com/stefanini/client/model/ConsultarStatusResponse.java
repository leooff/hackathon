package br.com.stefanini.client.model;

/**
 * @author Bradley
 *
 */
public class ConsultarStatusResponse {
		
	private boolean lampada1;
	
	private boolean lampada2;
	
	private boolean lampada3;
	
	private boolean lampada4;
	
	private boolean lampada5;
	
	private boolean lampada6;
	
	private boolean lampada7;
	
	private boolean lampada8;	
	
	private int dhtTemp;
	
	private int dhtUmid;
	
	private int ldrLum;
	
	private Double lm35;

	public int getDhtTemp() {
		return dhtTemp;
	}

	public void setDhtTemp(int dhtTemp) {
		this.dhtTemp = dhtTemp;
	}

	public int getDhtUmid() {
		return dhtUmid;
	}

	public void setDhtUmid(int dhtUmid) {
		this.dhtUmid = dhtUmid;
	}

	public int getLdrLum() {
		return ldrLum;
	}

	public void setLdrLum(int ldrLum) {
		this.ldrLum = ldrLum;
	}

	public boolean isLampada1() {
		return lampada1;
	}

	public void setLampada1(boolean lampada1) {
		this.lampada1 = lampada1;
	}

	public boolean isLampada2() {
		return lampada2;
	}

	public void setLampada2(boolean lampada2) {
		this.lampada2 = lampada2;
	}

	public boolean isLampada3() {
		return lampada3;
	}

	public void setLampada3(boolean lampada3) {
		this.lampada3 = lampada3;
	}

	public boolean isLampada4() {
		return lampada4;
	}

	public void setLampada4(boolean lampada4) {
		this.lampada4 = lampada4;
	}

	public boolean isLampada5() {
		return lampada5;
	}

	public void setLampada5(boolean lampada5) {
		this.lampada5 = lampada5;
	}

	public boolean isLampada6() {
		return lampada6;
	}

	public void setLampada6(boolean lampada6) {
		this.lampada6 = lampada6;
	}

	public boolean isLampada7() {
		return lampada7;
	}

	public void setLampada7(boolean lampada7) {
		this.lampada7 = lampada7;
	}

	public boolean isLampada8() {
		return lampada8;
	}

	public void setLampada8(boolean lampada8) {
		this.lampada8 = lampada8;
	}
	
	public Double getLm35() {
		return lm35;
	}

	public void setLm35(Double lm35) {
		this.lm35 = lm35;
	}
	
	@Override
	public String toString() {
		return "ConsultarStatusResponse [lampada1=" + lampada1 + ", lampada2="
				+ lampada2 + ", lampada3=" + lampada3 + ", lampada4="
				+ lampada4 + ", lampada5=" + lampada5 + ", lampada6="
				+ lampada6 + ", lampada7=" + lampada7 + ", lampada8="
				+ lampada8 + ", dhtTemp=" + dhtTemp + ", dhtUmid=" + dhtUmid
				+ ", ldrLum=" + ldrLum + ", lm35=" + lm35 + "]";
	}

	public void parser(String msg){		
		msg.trim();
		String[] mensagem = msg.split("\\|");		
		this.lampada1 = Boolean.parseBoolean(mensagem[0]);
		this.lampada2 = Boolean.parseBoolean(mensagem[1]);
		this.lampada3 = Boolean.parseBoolean(mensagem[2]);
		this.lampada4 = Boolean.parseBoolean(mensagem[3]);
		this.lampada5 = Boolean.parseBoolean(mensagem[4]);
		this.lampada6 = Boolean.parseBoolean(mensagem[5]);
		this.lampada7 = Boolean.parseBoolean(mensagem[6]);
		this.lampada8 = Boolean.parseBoolean(mensagem[7]);		
		this.dhtTemp = Integer.parseInt(mensagem[8]);
		this.dhtUmid = Integer.parseInt(mensagem[9]);
		this.ldrLum = Integer.parseInt(mensagem[10]);
		this.lm35 = Double.parseDouble(mensagem[11]);
	}

}
