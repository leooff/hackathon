package br.com.stefanini.client.model;

public class SensorLuminosidadeResponse {

	private int luminosidade;

	public int getLuminosidade() {
		return luminosidade;
	}

	public void setLuminosidade(int luminosidade) {
		this.luminosidade = luminosidade;
	}

	@Override
	public String toString() {
		return "SensorLuminosidadeResponse [luminosidade=" + luminosidade + "]";
	}	
}
