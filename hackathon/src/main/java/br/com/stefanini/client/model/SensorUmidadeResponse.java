package br.com.stefanini.client.model;

public class SensorUmidadeResponse {

	private int umidadeAr;

	public int getUmidadeAr() {
		return umidadeAr;
	}

	public void setUmidadeAr(int umidadeAr) {
		this.umidadeAr = umidadeAr;
	}

	@Override
	public String toString() {
		return "SensorUmidadeResponse [umidadeAr=" + umidadeAr + "]";
	}
}
