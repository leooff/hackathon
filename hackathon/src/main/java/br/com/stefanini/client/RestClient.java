package br.com.stefanini.client;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.stefanini.client.model.AlteraLampadaRequest;
import br.com.stefanini.client.model.AlteraLampadaResponse;
import br.com.stefanini.client.model.ConsultaIndicadoresRequest;
import br.com.stefanini.client.model.ConsultaIndicadoresResponse;
import br.com.stefanini.client.model.ConsultarLampadaResponse;
import br.com.stefanini.client.model.ConsultarStatusResponse;
import br.com.stefanini.client.model.SensorLuminosidadeResponse;
import br.com.stefanini.client.model.SensorTemperaturaResponse;
import br.com.stefanini.client.model.SensorUmidadeResponse;

@Service
public class RestClient {
	
	//private static final String URL_BASE = "http://10.14.11.43:8080/DomoticaControl/rest/";
	private static final String URL_BASE = "http://10.14.11.43:8080/DomoticaControl/rest/mock/";
	
	private RestTemplate restTemplate;
	
	public RestClient() {
		restTemplate = new RestTemplate();
	}
	
	// CONSULTAR STATUS GERAL
	public ConsultarStatusResponse consultarStatusGeral() {
		return restTemplate.getForObject(URL_BASE + "status", ConsultarStatusResponse.class);
	}
	
	// ACENDER LAMPADA
	public AlteraLampadaResponse alterarStatusLampada(final AlteraLampadaRequest request) {
		return restTemplate.postForObject(URL_BASE + "lampada", request, AlteraLampadaResponse.class);
	}
	
	// HISTORICO
	public ConsultaIndicadoresResponse consultarHistoricoSensor(final ConsultaIndicadoresRequest request) {
		return restTemplate.postForObject(URL_BASE + "indicadores", request, ConsultaIndicadoresResponse.class);
	}
	
	// CONSULTA TEMPERATURA
	public SensorTemperaturaResponse consultarTemperatura() {
		// TODO IMPLEMENTAR
		return null;
	}
	
	// CONSULTA UMIDADE
	public SensorUmidadeResponse consultarUmidadeAr() {
		// TODO IMPLEMENTAR
		return null;
	}
	
	// CONSULTA LUMINOSIDADE
	public SensorLuminosidadeResponse consultarLuminosidade() {
		// TODO IMPLEMENTAR
		return null;
	}
	
	// CONSULTA LAMPADA
	public ConsultarLampadaResponse consultarStatusLampada(final int idLampada) {
		// TODO IMPLEMENTAR
		return null;
	}
	
	public static void main(String[] args) {		
		
		//Exemplo requisicao GET sem parametros
		RestTemplate restTemplate = new RestTemplate();		
		ConsultarStatusResponse response = restTemplate.getForObject(URL_BASE + "status", ConsultarStatusResponse.class);
		System.out.println(response.toString());
		
		//Exemplo requisicao GET com parametros
		int idLampada = 5;
		ConsultarLampadaResponse estadoResponse = restTemplate.getForObject(URL_BASE + "lampada/" + idLampada, ConsultarLampadaResponse.class);
		System.out.println(estadoResponse.getEstadoLampada());
		
		//Exemplo requisicao POST
		AlteraLampadaRequest request = new AlteraLampadaRequest();
		request.setIdLampada(5);
		AlteraLampadaResponse lampResponse = restTemplate.postForObject(URL_BASE + "lampada", request, AlteraLampadaResponse.class);
		System.out.println(lampResponse.getEstadoLampada());
		
		//Buscar indicadores
		ConsultaIndicadoresRequest indicadoresRequest = new ConsultaIndicadoresRequest();
		indicadoresRequest.setSensor("lm35");
		indicadoresRequest.setDataInicio("22/03/2018");
		indicadoresRequest.setDataFim("22/03/2018");
		ConsultaIndicadoresResponse indicadoresResponse = restTemplate.postForObject(URL_BASE + "indicadores", indicadoresRequest, ConsultaIndicadoresResponse.class);
		System.out.println(indicadoresResponse.toString());
	}

}
